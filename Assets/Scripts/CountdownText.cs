﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownText : MonoBehaviour
{
    public delegate void CountdownDelegate();
    public static event CountdownDelegate OnCountdownFinished;
    Text countdownText;

    void OnEnable()
    {
        countdownText = GetComponent<Text>();
        countdownText.text = "3";
        StartCoroutine("Countdown");
    }

    IEnumerator Countdown()
    {
        int count = 3;
        for (int i = count; i > 0; i--)
        {
            countdownText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        OnCountdownFinished();
    }
}
