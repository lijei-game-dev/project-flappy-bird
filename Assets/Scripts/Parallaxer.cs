﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxer : MonoBehaviour
{
    [System.Serializable]
    public struct YSpawnRange
    {
        public float minY;
        public float maxY;
    }

    public GameObject prefab;
    public float shiftSpeed;
    public float spawnRate;
    public bool useSpawnRange;
    public YSpawnRange ySpawnRange;
    public Vector3 defaultSpawnPos;
    public bool spawnImmediate;
    public Vector3 immediateSpawnPos;
    public Vector2 targetAspectRatio;

    List<Transform> spawnObjectsList = new List<Transform>();
    float spawnTimer;
    float targetAspect;
    Vector3 dynamicSpawnPos;
    GameManager game;

    void Start()
    {
        game = GameManager.Instance;
        targetAspect = (float)targetAspectRatio.x / targetAspectRatio.y;
        dynamicSpawnPos.x = (defaultSpawnPos.x * Camera.main.aspect) / targetAspect;
        dynamicSpawnPos.y = (defaultSpawnPos.y * Camera.main.aspect) / targetAspect;
        if (spawnImmediate)
        {
            SpawnImmediate();
        }
    }

    void OnEnable()
    {
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    void OnDisable()
    {
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }

    void OnGameOverConfirmed()
    {
        spawnTimer = 0;
        for (int i = spawnObjectsList.Count - 1; i >= 0; i--)
        {
            GameObject go = spawnObjectsList[i].gameObject;
            spawnObjectsList.RemoveAt(i);
            Destroy(go);
        }

        if (spawnImmediate)
        {
            SpawnImmediate();
        }

    }

    void Spawn()
    {
        GameObject go = Instantiate(prefab) as GameObject;
        go.transform.SetParent(transform);
        Vector3 pos = Vector3.zero;
        pos.x = dynamicSpawnPos.x;
        if (useSpawnRange)
        {
            pos.y = Random.Range(ySpawnRange.minY, ySpawnRange.maxY);
        }
        else
        {
            pos.y = dynamicSpawnPos.y;
        }
        go.transform.position = pos;
        spawnObjectsList.Add(go.transform);
    }

    void SpawnImmediate()
    {
        GameObject go = Instantiate(prefab) as GameObject;
        go.transform.SetParent(transform);
        Vector3 pos = Vector3.zero;
        pos.x = (immediateSpawnPos.x * Camera.main.aspect) / targetAspect;
        pos.y = (immediateSpawnPos.y * Camera.main.aspect) / targetAspect;
        go.transform.position = pos;
        spawnObjectsList.Add(go.transform);
        Spawn();
    }

    void Shift()
    {
        for (int i = spawnObjectsList.Count - 1; i >= 0; i--)
        {
            spawnObjectsList[i].position -= Vector3.right * shiftSpeed * Time.deltaTime;
            if (spawnObjectsList[i].position.x < -dynamicSpawnPos.x)
            {
                GameObject go = spawnObjectsList[i].gameObject;
                spawnObjectsList.RemoveAt(i);
                Destroy(go);
            }
        }
    }

    void Update()
    {
        if (game.GameOver) return;

        dynamicSpawnPos.x = (defaultSpawnPos.x * Camera.main.aspect) / targetAspect;
        dynamicSpawnPos.y = (defaultSpawnPos.y * Camera.main.aspect) / targetAspect;

        spawnTimer += Time.deltaTime;
        if (spawnTimer > spawnRate)
        {
            spawnTimer = 0;
            Spawn();
        }
        Shift();
    }
}
