﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BallMovement : MonoBehaviour
{
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnPlayerScored;
    public static event PlayerDelegate OnPlayerDead;
    public float force = 150;
    public float tiltSmooth = 3;
    public Vector3 startPos;
    Rigidbody2D rigidBody;
    Quaternion downRotation;
    Quaternion forwardRotation;
    GameManager game;

    void OnEnable()
    {
        GameManager.OnGameStarted += OnGameStarted;
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    void OnDisable()
    {
        GameManager.OnGameStarted -= OnGameStarted;
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }

    void OnGameStarted()
    {
        rigidBody.velocity = Vector3.zero;
        rigidBody.simulated = true;
    }

    void OnGameOverConfirmed()
    {
        transform.localPosition = startPos;
        transform.rotation = Quaternion.identity;
    }

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.simulated = false;
        game = GameManager.Instance;
        downRotation = Quaternion.Euler(0, 0, -100);
        forwardRotation = Quaternion.Euler(0, 0, 40);
    }

    void Update()
    {
        if (game.GameOver) return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidBody.velocity = Vector2.zero;
            transform.rotation = forwardRotation;
            rigidBody.AddForce(Vector2.up * force);
        }
        transform.rotation = Quaternion.Lerp(transform.rotation, downRotation, tiltSmooth * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "ScoreZone")
        {
            OnPlayerScored();
        }
        if (collider.gameObject.tag == "DeadZone")
        {
            rigidBody.simulated = false;
            OnPlayerDead();
        }
    }
}
