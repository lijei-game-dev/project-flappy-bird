﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public delegate void GameDelegate();
    public static event GameDelegate OnGameStarted;
    public static event GameDelegate OnGameOverConfirmed;
    public GameObject startPage;
    public GameObject gameOverPage;
    public GameObject countDownPage;
    public Text scoreText;
    enum PageState
    {
        None,
        Start,
        GameOver,
        CountDown
    };
    int score = 0;
    bool gameOver = true;

    public bool GameOver { get { return gameOver; } }

    void Awake()
    {
        Instance = this;
        setPageState(PageState.Start);
    }

    void OnEnable()
    {
        BallMovement.OnPlayerScored += OnPlayerScored;
        BallMovement.OnPlayerDead += OnPlayerDead;
        CountdownText.OnCountdownFinished += OnCountdownFinished;
    }

    void OnDisable()
    {
        BallMovement.OnPlayerScored -= OnPlayerScored;
        BallMovement.OnPlayerDead -= OnPlayerDead;
        CountdownText.OnCountdownFinished -= OnCountdownFinished;
    }

    void OnPlayerScored()
    {
        score++;
        scoreText.text = score.ToString();
    }

    void OnPlayerDead()
    {
        gameOver = true;
        int highscore = PlayerPrefs.GetInt("Highscore");
        if (score > highscore)
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
        setPageState(PageState.GameOver);
    }

    void OnCountdownFinished()
    {
        gameOver = false;
        OnGameStarted();
        setPageState(PageState.None);
        score = 0;
    }

    void setPageState(PageState pageState)
    {
        switch (pageState)
        {
            case PageState.None:
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countDownPage.SetActive(false);
                break;
            case PageState.Start:
                startPage.SetActive(true);
                gameOverPage.SetActive(false);
                countDownPage.SetActive(false);
                break;
            case PageState.GameOver:
                startPage.SetActive(false);
                gameOverPage.SetActive(true);
                countDownPage.SetActive(false);
                break;
            case PageState.CountDown:
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countDownPage.SetActive(true);
                break;
        }
    }

    public void StartGame()
    {
        setPageState(PageState.CountDown);
    }

    public void RestartGame()
    {
        setPageState(PageState.Start);
        scoreText.text = "0";
        OnGameOverConfirmed();
    }
}
